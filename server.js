const express = require('express');
const dotenv = require('dotenv');
let bodyParser = require('body-parser');
const create = require('./create.js');
const app = express();
const PORT = process.env.PORT || 5000;
const fs = require("fs");

app.use(bodyParser.json({ limit: '100000mb' }));
app.use(bodyParser.urlencoded({ limit: '1000000mb', extended: true }));

app.use(express.static(__dirname + '/public'));
app.listen(PORT, console.log(`running ${PORT}`));

app.get("/", (req, res) => {
    res.sendFile(__dirname + '/app/views/index.html');
});

app.get("/json", (req, res) => {
    fs.writeFileSync('output.json', req.query.json);
    res.end();
});




