import {
  json
} from "./json.js";
const url = "./json/input.json";
let elements = [],
  boxArr,
  view = $("#view"),
  click = false,
  order = 0,
  placeholder = 0;

$.getJSON(url, function (data) {
  boxArr = json(data);
});

document.addEventListener("mousedown", function (e) {
  x1 = e.clientX;
  y1 = e.clientY;
  order = 0;
  placeholder = 0;
  area.hidden = 0;
  reCalc();
  click = true;

  if (e.target.tagName !== "INPUT") {
    $(".visual-container").remove();
    elements.length = 0;
  }
});

document.addEventListener("mousemove", (e) => {
  x2 = e.clientX;
  y2 = e.clientY;

  if (click === true) {
    let multiple = $("#multiple"),
      areaW = parseInt(multiple.css("width")),
      areaH = parseInt(multiple.css("height")),
      areaL = parseInt(multiple.css("left")),
      areaT = parseInt(multiple.css("top")),
      areaR = areaW + areaL,
      areaB = areaT + areaH;

    boxArr.forEach((el) => {
      if (
        ((areaL <= el.left &&
            areaR >= el.left &&
            areaT <= el.top + el.height &&
            areaB >= el.top) ||
          (el.top <= areaB &&
            el.left + el.width >= areaL &&
            el.left + el.width <= areaR &&
            el.top >= areaT)) &&
        el.selected === undefined
      ) {
        const div = `<div class="visual-container selected" data-index=${
          el.index
        }; data-order=${order++} style="left: ${el.left}px; top: ${
          el.top
        }px; width: ${el.width}px; height: ${el.height}px; ">
            <input  placeholder=${placeholder++}>
          </div>`;

        view.append(div);
        el.selected = true;
      }
    });
  }

  reCalc();
});

document.addEventListener("mouseup", function () {
  area.hidden = 1;
  click = false;
  boxArr.forEach((el) => {
    if (el.selected === true) {
      el.selected = undefined;
      elements.push(el);
    }
  });
});

$("body").on("input", function (e) {
  $("input").each(function (index, el) {
    if (el === e.target) {
      $(`[data-order = '${e.originalEvent.data}']`)
        .children()
        .attr("placeholder", $(e.target).parent().attr("data-order"));

      $(`[data-order = '${e.originalEvent.data}']`).attr(
        "data-order",
        $(e.target).parent().attr("data-order")
      );

      $(e.target).parent().attr("data-order", e.originalEvent.data);
      $(e.target).attr("placeholder", e.originalEvent.data);
    }
  });
});

document.body.addEventListener("keydown", function (e) {
  if (e.keyCode == 67 && e.ctrlKey) {
    output();
  }
});

let area = document.getElementById("multiple"),
  x1 = 0,
  y1 = 0,
  x2 = 0,
  y2 = 0;

function reCalc() {
  var x3 = Math.min(x1, x2);
  var x4 = Math.max(x1, x2);
  var y3 = Math.min(y1, y2);
  var y4 = Math.max(y1, y2);
  area.style.left = x3 + "px";
  area.style.top = y3 + "px";
  area.style.width = x4 - x3 + "px";
  area.style.height = y4 - y3 + "px";
}

function output() {
  let outputJson = [];
  let jsonIndex = [];
  let orderIndex = $("[data-order]");
  let sortable = [];
  orderIndex.sort((a, b) => a - b).each(function (index, el) {
    sortable.push(parseInt(el.dataset.order));
  });

  sortable.forEach((el) => {
    jsonIndex.push(elements[el]);
  });

  let imgCount = 0;
  jsonIndex.forEach((el) => {
    let output = {};

    if (el.image) {

      function dataURLtoFile(dataurl, filename) {

        let arr = dataurl.split(','),
          mime = arr[0].match(/:(.*?);/)[1],
          bstr = atob(arr[1]),
          n = bstr.length,
          u8arr = new Uint8Array(n);
        while (n--) {
          u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], filename, {
          type: mime
        });
      }

      let file = dataURLtoFile(`data:image/png;base64,${el.image}`, `${imgCount++}.png`);

      output["img_path"] = file.name;
    } else {
      output["text"] = el.text.join("");
    }
    outputJson.push(output);

  });

  $.ajax({
    type: "GET",
    contentType: "application/json",
    url: "/json",
    data: {
      json: JSON.stringify(outputJson),
    },
  });
}