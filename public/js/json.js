export function json(data) {
  let w = data.pages[0].width,
    h = data.pages[0].height,
    arr = data.pages[0].blocks,
    boxArr = [];

  $("#pdf").attr({
    width: w,
    height: h
  });

  arr.forEach((e) => {
    let box = {},
      spans = [];

    box["left"] = e.bbox[0];
    box["top"] = e.bbox[1];
    box["right"] = e.bbox[2];
    box["bottom"] = e.bbox[3];
    box["width"] = e.bbox[2] - e.bbox[0];
    box["height"] = e.bbox[3] - e.bbox[1];
    box["index"] = e.number;

    if (e.lines) {
      e.lines.forEach((el) => {
        spans.push(el.spans[0].text);
        box["text"] = spans;
      });
    } else {
      box["image"] = e.image;
    }
    boxArr.push(box);
  });

  return boxArr;
}
